# Feature list
## Sorting of file storages
* Description
  * Control of file storage order, where file storages are shown. For example in the file list module.
* Usage
  * Configure through extension settings 
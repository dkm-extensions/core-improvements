<?php
namespace DKM\CoreImprovements\Hooks;
use DKM\FormMultiSite\Helpers\MiscHelper;
use DKM\FormMultiSite\Resource\Filter\FormDefinitionsFolderFilter;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * The purpose is so that file storages can be sorted
 *
 * Class GetPagePermsClause
 * @package DKM\CoreImprovements\Hooks
 */
class GetPagePermsClause
{
    public static $filterWhenRoute = ['/folder/tree', '/module/file/FilelistList', '/wizard/record/browse', '/ajax/filestorage/tree/fetchData', '/ajax/filestorage/tree/filterData'];

    /**
     * @param $params
     * @param $parentObject BackendUserAuthentication
     * @return mixed
     */
    public static function main($params, BackendUserAuthentication $parentObject) {
        try {
            $extSettings = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('core_improvements');
        } catch (ExtensionConfigurationExtensionNotConfiguredException $e) {
        } catch (ExtensionConfigurationPathDoesNotExistException $e) {
        }
        if($extSettings['storageOrder'] ?? false) {
            $order = GeneralUtility::trimExplode(',', $extSettings['storageOrder']);
            $order = array_flip($order);
            if($_GET['route'] ?? false) {
                $route = $_GET['route'];
            } else {
                $requestURI = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REQUEST_URI');
                $route = str_replace('/typo3', '', $requestURI);
                $route = strtok($route, '?');
            }
            if(in_array($route, self::$filterWhenRoute)) {
                $parentObject->getFileMountRecords();
                /** @var VariableFrontend $runtimeCache */
                $runtimeCache = GeneralUtility::makeInstance(CacheManager::class)->getCache('runtime');
                $fileMountRecordCache = $runtimeCache->get('backendUserAuthenticationFileMountRecords') ?: [];
                usort($fileMountRecordCache, function ($a, $b) use ($order) {
                    return strcmp($order[$a['base']] ?? 100000, $order[$b['base']] ?? 100000);
                });
                $runtimeCache->set('backendUserAuthenticationFileMountRecords', $fileMountRecordCache);
            }
        }
        return $params['currentClause'];
    }
}
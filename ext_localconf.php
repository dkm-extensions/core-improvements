<?php
if (!defined('TYPO3')) {
    defined('TYPO3') || die('Access denied.');
}
(function() {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_userauthgroup.php']['getPagePermsClause']['coreImprovements'] = \DKM\CoreImprovements\Hooks\GetPagePermsClause::class . '->main';
})();

<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Core improvements',
    'description' => 'Different improvements to the TYPO3 core, that can be activated individually',
    'version' => '0.1.0',
    'state' => 'alpha',
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dkm.dk',
    'author_company' => 'Danish Church Mediacenter',
];
